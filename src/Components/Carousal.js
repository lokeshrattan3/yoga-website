import React from 'react'
import { Link } from 'react-router-dom'
import img from '../images/first.png'

function Carousal() {
  return (
    <div>
        <div className="carousal row justify-content-center">
            <div className="col-md-6 col-sm-12">
                <img src={img} alt="img" className="first_img" />
            </div>
            <div className="col-md-6 col-sm-12 first_heading">
                <h1 className="heading">YOGA CLASS</h1>
                <p className="text">
                Lorem ipsum dolor sit amet, consectetur adipi-
                <br />
                scing elit.Donec fringilla neque euismod volu-
                <br />
                tpat cursus. Vestibulum ac orci.
                </p>
                <div className="button">
                <button><Link className="text-wrap row" to={"/Login"} style={{textDecoration:"none",color:"#ffffff"}}>JOIN NOW</Link></button>
                </div>
            </div>
        </div>
    </div>
  )
}

export default Carousal