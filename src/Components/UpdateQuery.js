import React, { useEffect, useState } from 'react';
import './contact.css';
import Layout from '../Layout';
import axios from 'axios';
import Cookies from 'js-cookie';
import { useParams } from 'react-router-dom';

const UpdateQuery = () => {
  const {id} = useParams();
  const [data, setData] = useState([]);
  const userId = Cookies.get('userId');
  

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(`http://localhost:5000/yoga-api/contact-us/${userId}`);
        const result = await response.json();
        setData(result.data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, [userId]);

  const [formData, setFormData] = useState({
    name: data?.name,
    email: data?.email,
    message: data?.message,
  });
  const [errors, setErrors] = useState({});

  const validateForm = () => {
    const { name, email, message } = formData;
    const newErrors = {};

    if (!name) {
      newErrors.name = 'Name is required';
    }

    if (!email || !/^\S+@\S+\.\S+$/.test(email)) {
      newErrors.email = 'Valid email is required';
    }

    if (!message) {
      newErrors.message = 'Message is required';
    }

    return newErrors;
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const newErrors = validateForm();
    
    if (Object.keys(newErrors).length === 0) {
      // Submit the form or send the data to your server.
      axios.post('http://localhost:5000/yoga-api/update-query/'+id, formData)
      .then((res) => {
        // if(res.data.status === 'success') {
          alert("your query updated successfully")
        // }
      })
      .catch((err) => {
        alert(err.data.Error)
      });
      
    } else {
      setErrors(newErrors);
    }
  };

  const handleChange = (e) => {
    setFormData((prevData) => ({
      ...prevData,
      [e.target.name]: e.target.value,
    }));
  };

  const { name, email, message } = formData;
  // const isLoggedIn = window.localStorage.getItem("isLoggedIn");

  return (
    <Layout>
      <div className="contact-us-container container-fluid">
        <form onSubmit={handleSubmit} className="container">
          <h2>Update Your Query</h2>
          <div className="form-group">
            <input
              type="text"
              name="name"
              placeholder="Name"
              value={name}
              onChange={handleChange}
            />
            {errors.name && <span className="error">{errors.name}</span>}
          </div>
          <div className="form-group">
            <input
              type="email"
              name="email"
              placeholder="Email"
              value={email}
              onChange={handleChange}
            />
            {errors.email && <span className="error">{errors.email}</span>}
          </div>
          <div className="form-group">
            <textarea
              name="message"
              placeholder="Message"
              value={message}
              onChange={handleChange}
            />
            {errors.message && <span className="error">{errors.message}</span>}
          </div>

          {/* {isLoggedIn ? */}
            <button type="submit">Submit</button>

          {/* : <button onClick={()=>alert("You need to login first")} type='button'>Submit</button>
          } */}
          
        </form>
      </div>
    </Layout>
  );
};

export default UpdateQuery;
