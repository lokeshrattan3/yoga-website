import React, { useState } from 'react';
import './contact.css';
import Layout from '../Layout';
import axios from 'axios';
import Cookies from 'js-cookie';
import { useNavigate } from 'react-router-dom';

const PersonalInfo = () => {
  const userId = Cookies.get('userId');
  const navigate = useNavigate()
  const [formData, setFormData] = useState({
    name: '',
    weight: '',
    height: '',
    uid: userId
  });

  const [errors, setErrors] = useState({});

  const validateForm = () => {
    const { name, weight, height } = formData;
    const newErrors = {};

    if (!name) {
      newErrors.name = 'Name is required';
    }

    if (!weight) {
      newErrors.weight = 'weight is required';
    }

    if (!height) {
      newErrors.height = 'height is required';
    }

    return newErrors;
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const newErrors = validateForm();
    
    if (Object.keys(newErrors).length === 0) {
      // Submit the form or send the data to your server.
      axios.post('http://localhost:5000/yoga-api/personal-info', formData)
      .then((res) => {
        if(res.data.status === 'success') {
          alert("your Info Added Successfully");
          navigate('/my-profile')
          console.log('Form data:', formData);
        }
      })
      .catch((err) => {
        alert(err.data.Error)
        console.log('Error', err);
      });
      
    } else {
      setErrors(newErrors);
    }
  };

  const handleChange = (e) => {
    setFormData((prevData) => ({
      ...prevData,
      [e.target.name]: e.target.value,
    }));
  };

  const { name, weight, height } = formData;
  const isLoggedIn = window.localStorage.getItem("isLoggedIn");

  return (
    <Layout>
      <div className="contact-us-container container-fluid">
        <form onSubmit={handleSubmit} className="container">
          <h2>Add Personal Info</h2>
          <div className="form-group">
            <input
              type="text"
              name="name"
              placeholder="Name"
              value={name}
              onChange={handleChange}
            />
            {errors.name && <span className="error">{errors.name}</span>}
          </div>
          <div className="form-group">
            <input
              name="weight"
              placeholder="weight"
              value={weight}
              onChange={handleChange}
            />
            {errors.weight && <span className="error">{errors.weight}</span>}
          </div>
          <div className="form-group">
            <input
              name="height"
              placeholder="height"
              value={height}
              onChange={handleChange}
            />
            {errors.height && <span className="error">{errors.height}</span>}
          </div>

          {isLoggedIn ?
            <button type="submit">Submit</button>

          : <button onClick={()=>alert("You need to login first")} type='button'>Submit</button>
          }
          
        </form>
      </div>
    </Layout>
  );
};

export default PersonalInfo;
