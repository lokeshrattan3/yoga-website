import React from 'react'
import { Link } from 'react-router-dom'
import LOGO from '../images/Logo.png'

function Header(props) {
  return (
    <div>
        <header className="header">
        <nav className="navbar navbar-expand-lg">
          <div className="container-fluid">
            <div className="logo navbar-brand">
              <img src={LOGO} alt="logo" className="img-fluid" />
            </div>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
              <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <Link className="nav-link active" aria-current="page" to={"/"}>
                    Home
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to={"/about"}>
                    About Us
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to={"/service"}>
                    Services
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to={"/contact"}>
                    Contact Us
                  </Link>
                </li>
                <li className="nav-item" style={{
                  // background: 'black',
                        display: "flex", 
                        justifyContent: 'center', 
                        alignItems: 'center',}} >
                  {props.children}
                  {/* <Link className="nav-link" to={"/login"}>
                    Login
                  </Link> */}
                </li>
              </ul>
            </div>
          </div>
        </nav>
        
      </header>
    </div>
  )
}

export default Header