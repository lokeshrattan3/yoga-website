import React, { useEffect, useState } from 'react';
import './contact.css';
import Layout from '../Layout';
import axios from 'axios';
import Cookies from 'js-cookie';
import { useNavigate, useParams } from 'react-router-dom';

const UpdateInfo = () => {
  const {id} = useParams();
  const [data, setData] = useState([]);
  const userId = Cookies.get('userId');
  const navigate = useNavigate()

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(`http://localhost:5000/yoga-api/personal-info/${userId}`);
        const result = await response.json();
        setData(result.data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, [userId]);

  const [formData, setFormData] = useState({
    name: data?.name,
    weight: data?.weight,
    height: data?.height,
  });
  const [errors, setErrors] = useState({});

  const validateForm = () => {
    const { name, weight, height } = formData;
    const newErrors = {};

    if (!name) {
      newErrors.name = 'Name is required';
    }

    if (!weight) {
      newErrors.weight = 'weight is required';
    }

    if (!height) {
      newErrors.height = 'height is required';
    }

    return newErrors;
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const newErrors = validateForm();
    
    if (Object.keys(newErrors).length === 0) {
      // Submit the form or send the data to your server.
      axios.post('http://localhost:5000/yoga-api/update-info/'+id, formData)
      .then((res) => {
        // if(res.data.status === 'success') {
          navigate('/my-profile')
          alert("your personal info updated successfully")
        // }
      })
      .catch((err) => {
        alert(err.data.Error)
      });
      
    } else {
      setErrors(newErrors);
    }
  };

  const handleChange = (e) => {
    setFormData((prevData) => ({
      ...prevData,
      [e.target.name]: e.target.value,
    }));
  };

  const { name, weight, height } = formData;
  // const isLoggedIn = window.localStorage.getItem("isLoggedIn");

  return (
    <Layout>
      <div className="contact-us-container container-fluid">
        <form onSubmit={handleSubmit} className="container">
          <h2>Update Your Personal Info</h2>
          <div className="form-group">
            <input
              type="text"
              name="name"
              placeholder="Name"
              value={name}
              onChange={handleChange}
            />
            {errors.name && <span className="error">{errors.name}</span>}
          </div>
          <div className="form-group">
            <input
              type="weight"
              name="weight"
              placeholder="weight"
              value={weight}
              onChange={handleChange}
            />
            {errors.weight && <span className="error">{errors.weight}</span>}
          </div>
          <div className="form-group">
            <input
              name="height"
              placeholder="height"
              value={height}
              onChange={handleChange}
            />
            {errors.height && <span className="error">{errors.height}</span>}
          </div>

          {/* {isLoggedIn ? */}
            <button type="submit">Submit</button>

          {/* : <button onClick={()=>alert("You need to login first")} type='button'>Submit</button>
          } */}
          
        </form>
      </div>
    </Layout>
  );
};

export default UpdateInfo;
