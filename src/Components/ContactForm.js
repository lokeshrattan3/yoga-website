import React, { useState } from 'react';
import './contact.css';
import Layout from '../Layout';
import axios from 'axios';
import Cookies from 'js-cookie';
import { useNavigate } from 'react-router-dom';

const ContactUsForm = () => {
  const userId = Cookies.get('userId');
  const navigate = useNavigate()
  const [formData, setFormData] = useState({
    name: '',
    email: '',
    message: '',
    uid: userId
  });

  const [errors, setErrors] = useState({});

  const validateForm = () => {
    const { name, email, message } = formData;
    const newErrors = {};

    if (!name) {
      newErrors.name = 'Name is required';
    }

    if (!email || !/^\S+@\S+\.\S+$/.test(email)) {
      newErrors.email = 'Valid email is required';
    }

    if (!message) {
      newErrors.message = 'Message is required';
    }

    return newErrors;
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const newErrors = validateForm();
    
    if (Object.keys(newErrors).length === 0) {
      // Submit the form or send the data to your server.
      axios.post('http://localhost:5000/yoga-api/contact-us', formData)
      .then((res) => {
        if(res.data.status === 'success') {
          alert("your query sent it successfully")
          navigate('/my-profile')
          console.log('Form data:', formData);
        }
      })
      .catch((err) => {
        alert(err.data.Error)
        console.log('Error', err);
      });
      
    } else {
      setErrors(newErrors);
    }
  };

  const handleChange = (e) => {
    setFormData((prevData) => ({
      ...prevData,
      [e.target.name]: e.target.value,
    }));
  };

  const { name, email, message } = formData;
  const isLoggedIn = window.localStorage.getItem("isLoggedIn");

  return (
    <Layout>
      <div className="contact-us-container container-fluid">
        <form onSubmit={handleSubmit} className="container">
          <h2>Contact Us</h2>
          <div className="form-group">
            <input
              type="text"
              name="name"
              placeholder="Name"
              value={name}
              onChange={handleChange}
            />
            {errors.name && <span className="error">{errors.name}</span>}
          </div>
          <div className="form-group">
            <input
              type="email"
              name="email"
              placeholder="Email"
              value={email}
              onChange={handleChange}
            />
            {errors.email && <span className="error">{errors.email}</span>}
          </div>
          <div className="form-group">
            <textarea
              name="message"
              placeholder="Message"
              value={message}
              onChange={handleChange}
            />
            {errors.message && <span className="error">{errors.message}</span>}
          </div>

          {isLoggedIn ?
            <button type="submit">Submit</button>

          : <button onClick={()=>alert("You need to login first")} type='button'>Submit</button>
          }
          
        </form>
      </div>
    </Layout>
  );
};

export default ContactUsForm;
