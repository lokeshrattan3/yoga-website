export default function Class(){
    return(
        <section className="choose_class">
        <div className="container-lg">
          <div className="row">
            <div className="title-txt">
              <p className="heading2">CHOOSE CLASS</p>
              <p className="text2">
                Lorem ipsum dolor sit amet, consectetur adipisc-
                <br />
                ing elit. Vivamus lacinia odio vitae Vestibulum ves-
                <br />
                tibulum.
              </p>
            </div>
          </div>
          <div className="row">
            <div className="col-md-4 choose1">
              <img src={require("../images/choose1.png")} />
              <h1 className="new">NEWBIE CLASS</h1>
              <p className="des1">
                Lorem ipsum dolor sit amet, con-
                <br />
                sectetur adipiscing elit.
              </p>
            </div>
            <div className="col-md-4 choose2">
              <img src={require("../images/choose2.png")} />
              <h1 className="new">ADVANCE CLASS</h1>
              <p className="des1">
                Lorem ipsum dolor sit amet, con-
                <br />
                sectetur adipiscing elit.
              </p>
            </div>
            <div className="col-md-4 choose2">
              <img src={require("../images/choose3.png")} />
              <h1 className="new">EXPERT CLASS</h1>
              <p className="des1">
                Lorem ipsum dolor sit amet, con-
                <br />
                sectetur adipiscing elit.
              </p>
            </div>
          </div>
        </div>
      </section>
    )
}