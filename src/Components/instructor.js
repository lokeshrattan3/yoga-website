export default function Instructor(){
    return(
        <section className="instructor">
        <div className="container">
          <div className="row">
            <div className="col-md-6 col-sm-12">
              <h1 className="h1_text">
                BEST
                <br />
                INSTRUCTOR
              </h1>
              <p className="instructor_text">
                Lorem ipsum dolor sit amet, consecte-
                <br />
                tur adipiscing elit. Vivamus lacinia
                <br /> odio vitae Vestibulum vestibulum.
              </p>
              <div className="button2">
                <button>READ MORE</button>
              </div>
            </div>
            <div className="col-md-6 col-sm-12 img-col">
              <img
                src={require("../images/best_img.png")}
                className=" best_img"
              />
            </div>
          </div>
        </div>
      </section>
    )
}