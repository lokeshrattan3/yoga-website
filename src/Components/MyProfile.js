import React, { useEffect, useState } from 'react'
import Layout from '../Layout'
import Cookies from 'js-cookie';
import { Link } from 'react-router-dom';
import axios from 'axios';

function MyProfile() {

    const [data, setData] = useState([]);
    const [personalData, setPersonalData] = useState([]);
    const userId = Cookies.get('userId');

    useEffect(()=>{
        fetch(`http://localhost:5000/yoga-api/contact-us/`+userId)
        .then(res => res.json())
        // .then((getData) => setData(getData))
        .then((getData) => setData(getData?.data))
        .catch(err => console.log(err))
    },[])

    useEffect(()=>{
        fetch(`http://localhost:5000/yoga-api/personal-info/`+userId)
        .then(res => res.json())
        // .then((getData) => setData(getData))
        .then((getData) => setPersonalData(getData?.data))
        .catch(err => console.log(err))
    },[])


    const handleDelete = async () => {
        try {
            const response = await axios.delete(
            `http://localhost:5000/yoga-api/contact-us/delete` + userId,
            {
                headers: {
                'Content-Type': 'application/json',
                // Add any other headers as needed
                },
            }
            );
        
            // Check if the request was successful (status code 2xx)
            if (response.status >= 200 && response.status < 300) {
            window.location.reload(true);
            alert('Your data deleted successfully');
            } else {
            // Handle non-successful status codes (4xx, 5xx, etc.)
            console.error('Error deleting data:', response.statusText);
            }
        } catch (err) {
            console.error('Error:', err);
        }
    };


return (
    <>
        <Layout>

            <div className='container'>
                <div className='py-4'>
                <h3 className='text-center'>Here is your Personal information</h3>
                    <table className="table table-bordered table-striped table-hover">
                        <thead className=''>
                            {personalData?.length > 0 ? null : <tr className=''>
                                <td colSpan={6}><Link to={`/personal-info`} className='btn btn-outline-dark w-100'>Add your personal info</Link></td>
                            </tr>
                            }
                            
                            <tr>
                                <td className='text-center'>Name</td>
                                <td className='text-center'>Weight</td>
                                <td className='text-center'>Height</td>
                                <td className='text-center' colSpan={0}>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            {personalData?.map((itm, index) => {return <tr key={index}>
                                <td>{itm?.name}</td>
                                <td>{itm?.weight}</td>
                                <td>{itm?.height}</td>
                                <td className='text-center'><Link to={`/update-info/${itm?.id}`} className='btn btn-success'>Update</Link></td>
                                {/* <td className='text-center'><button type='button' className='btn btn-danger' onClick={handleDelete}>Delete</button></td> */}
                            </tr>}
                            )}
                        </tbody>
                    </table>
                </div>

                <div className='py-4'>
                    <h3 className='text-center'>Here is your Sent Queries</h3>
                    <table className="table table-bordered table-striped table-hover">
                        <thead className=''>
                            <tr className=''>
                                <td colSpan={6}><Link to={`/contact`} className='btn btn-outline-dark w-100'>Add new Query</Link></td>
                            </tr>
                            <tr>
                                <td className='text-center'>Sr. No</td>
                                <td className='text-center'>Name</td>
                                <td className='text-center'>Email</td>
                                <td className='text-center'>Message</td>
                                <td className='text-center' colSpan={2}>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            {data?.map((itm, index) => {return <tr key={index}>
                                <td>{index + 1}</td>
                                <td>{itm?.name}</td>
                                <td>{itm?.email}</td>
                                <td>{itm?.message}</td>
                                <td className='text-center'><Link to={`/update-query/${itm?.id}`} className='btn btn-success'>Update</Link></td>
                                <td className='text-center'><button type='button' className='btn btn-danger' onClick={handleDelete}>Delete</button></td>
                            </tr>}
                            )}
                        </tbody>
                    </table>
                </div>

            </div>

            
        </Layout>   
    </>
  )
}

export default MyProfile