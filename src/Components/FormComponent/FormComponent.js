import Stat from "../Stat"
import Experience from "../experience"
import Footer from "../footer"
import Instructor from "../instructor"
import Update from "../update"


export const About = () => {
    return (
        <>
            <Stat/>
            <Instructor/>
            <Experience/>
        </>
    )
}

export const Contact = () => {

    return (<>
        <Update/>
    </>)
}