export default function Stat(){
    return(
        <section className="stat">
        <div className="container">
          <div className="row">
            <div className="title-txt">
              <p className="h">OUR STATS</p>
              <p className="stat_text">
                Lorem ipsum dolor sit amet, consectetur adipisc-
                <br />
                ing elit. Vivamus lacinia odio vitae Vestibulum ves-
                <br />
                tibulum.{" "}
              </p>
            </div>
          </div>
          <div className="row justify-content-center">
            <div className="col-md-4 col-sm-12">
              <div className="p-3 mb-3 circleOne">
                <h1 className="num">123</h1>
                <p className="lorem">
                  Lorem ipsum dolor sit
                  <br /> amet.
                </p>
              </div>
            </div>
            <div className="col-md-4 col-sm-12">
              <div className="p-3 mb-3 circleOne">
                <h1 className="num">123</h1>
                <p className="lorem">
                  Lorem ipsum dolor sit
                  <br /> amet.
                </p>
              </div>
            </div>
            <div className="col-md-4 col-sm-12">
              <div className="p-3 mb-3 circleOne">
                <h1 className="num">123</h1>
                <p className="lorem">
                  Lorem ipsum dolor sit
                  <br /> amet.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
}