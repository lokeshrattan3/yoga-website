export default function Experience(){
    return(
        <section className="exp">
        <div className="container">
          <div className="row">
            <div className="col-md-6 col-sm-12">
              <img src={require("../images/new_img.png")} className="new_img" />
            </div>
            <div className="col-md-6 col-sm-12">
              <h1 className="h2_text">
                NEW
                <br />
                EXPERIENCE
              </h1>
              <p className="exp_text">
                Lorem ipsum dolor sit amet, consecte-
                <br />
                tur adipiscing elit. Vivamus lacinia
                <br /> odio vitae Vestibulum vestibulum.
              </p>
              <div className="button3">
                <button>READ MORE</button>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
}