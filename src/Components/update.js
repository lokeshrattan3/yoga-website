export default function Update(){
    return(
        <section className="update">
        <div className="container">
          <div className="row justify-content-center">
            <h2 className="update_text">DONT MISS OUR UPDATE</h2>
            <p className="update_lorem">
              Lorem ipsum dolor sit amet, consectetur adipisc-
              <br />
              ing elit. Vivamus lacinia odio vitae Vestibulum <br />
              vestibulum.
            </p>
            <div className="button4">
              YOUR EMAIL
              <a href="#">Subscribe</a>
            </div>
          </div>
        </div>
      </section>
    )
}