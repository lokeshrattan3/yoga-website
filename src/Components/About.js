import Layout from "../Layout";
import Stat from "./Stat";
import Experience from "./experience";
import Instructor from "./instructor";

export default function About(){
    return(
        <>
            <Layout>
                <Stat/>
                <Instructor/>
                <Experience/>
            </Layout>
            
        </>
    )
}