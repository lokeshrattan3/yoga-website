
export default function Footer(){
    return(
        <>
        <section className="footer">
        <div className="container">
          <div className="row">
            <div className="col-md-4 col-sm-12">
              <ul className="list1">
                <li className="fir">Title Here</li>
                <li className="sec">
                  Lorem ipsum dolor sit amet, conse-
                  <br />
                  ctetur adipiscing elit. Aliquam at dignis sim nunc, id maximus
                  ex. Etiam nec dignissim elit, at dignissim enim.{" "}
                </li>
              </ul>
            </div>
            <div className="col-md-3 col-sm-12">
              <ul className="list2">
                <li className="list_head">About</li>
                <li className="list_second">History</li>
                <li className="list_data">Our Team</li>
                <li className="list_data">Brand Guidlines</li>
                <li className="list_data">Terms & Conditions</li>
                <li className="list_data">Privacy Policy</li>
              </ul>
            </div>
            <div className="col-md-2 col-sm-12">
              <ul className="list2">
                <li className="list_head">Services</li>
                <li className="list_second">How To Order</li>
                <li className="list_data">Our Product</li>
                <li className="list_data">Order Status</li>
                <li className="list_data">Promo</li>
                <li className="list_data">Payment Method</li>
              </ul>
            </div>
            <div className="col-md-3 col-sm-12">
              <table>
                <tbody>
                <tr>
                  <td>
                    <img
                      src={require("../images/Social Media.png")}
                      className="footer_img" alt="Social Media"
                    />
                  </td>
                  <td>
                    <ul className="list2">
                      <li className="list_head">Follow</li>
                      <li className="facebook">Facebook</li>
                      <li className="twitter">Twitter</li>
                      <li className="insta">Instagram</li>
                      <li className="insta">Whatsapp</li>
                    </ul>
                  </td>
                </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </section>
      </>
    )
}