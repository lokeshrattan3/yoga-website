import axios from "axios";
import Carousal from "./Carousal";
import Class from "./Class";
import Header from "./Header";
import Pricing from "./pricing";
import Trainer from "./trainer";
import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { About, Contact } from "./FormComponent/FormComponent";
import Layout from "../Layout";

export default function Home(){

    return(
        <>  
            <Layout>
                <Carousal/>
                <Class/>
                <About/>
                <Pricing/>
                <Trainer/>
                <Contact/>
            </Layout>
            
        </>
        
    )
}

