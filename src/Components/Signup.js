import React, { useState } from 'react';
import './login.css';
import { Link, useNavigate } from 'react-router-dom';
import Layout from '../Layout';
import axios from 'axios';

const SignupPage = () => {
  const [formData, setFormData] = useState({
    name: '',
    email: '',
    password: '',
    confirmPassword: '',
  });

  const [errors, setErrors] = useState({
    name: '',
    email: '',
    password: '',
    confirmPassword: '',
  });

  const navigate = useNavigate();

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    // Form validation logic
    const { name, email, password, confirmPassword } = formData;
    const newErrors = {};

    if (!name) {
      newErrors.name = 'Name is required';
    }

    if (!email) {
      newErrors.email = 'Email is required';
    }

    if (!password) {
      newErrors.password = 'Password is required';
    }

    if (password !== confirmPassword) {
      newErrors.confirmPassword = 'Passwords do not match';
    }

    if (Object.keys(newErrors).length > 0) {
      setErrors(newErrors);
      return;
    }

    // If validation passes, proceed with signup logic
    axios.post('http://localhost:5000/yoga-api/signup', formData)
      .then((res) => {
        if (res.status === 200) {
          navigate('/login');
        }
        console.log(res);
        console.log('Signup successful');
      })
      .catch((err) => {
        console.error(err, 'Error in axios request');
      });
  };

  return (
    <Layout>
      <div className="" style={{ padding: '5em 0', background: '#6C9894' }}>
        <div className="form-container container-fluid">
          <h2>Signup</h2>
          <form onSubmit={handleSubmit} className="container" style={{ boxShadow: 'none' }}>
            <div className="form-group">
                <input
                  type="text"
                  name="name"
                  placeholder="Name"
                  value={formData?.name}
                  onChange={handleInputChange}
                />
                {errors.name && <div className="error">{errors.name}</div>}
              </div>
              <div className="form-group">
                <input
                  type="text"
                  name="email"
                  placeholder="Email"
                  value={formData?.email}
                  onChange={handleInputChange}
                />
                {errors.email && <div className="error">{errors.email}</div>}
              </div>
              <div className="form-group">
                <input
                  type="password"
                  name="password"
                  placeholder="Password"
                  value={formData?.password}
                  onChange={handleInputChange}
                  pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$" title="Password must contain at least one number, one lowercase letter, one uppercase letter, and be at least 8 characters long."
                />
                {errors.password && <div className="error">{errors.password}</div>}
              </div>
              <div className="form-group">
                <input
                  type="password"
                  name="confirmPassword"
                  placeholder="Confirm Password"
                  value={formData?.confirmPassword}
                  onChange={handleInputChange}
                  pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$" title="Password must contain at least one number, one lowercase letter, one uppercase letter, and be at least 8 characters long."
                />
                {errors.confirmPassword && <div className="error">{errors.confirmPassword}</div>}
              </div>
            <button type="submit">Signup</button>
            <br />
            <br />
            <Link to="/login">Already a member?</Link>
          </form>
        </div>
      </div>
    </Layout>
  );
};

export default SignupPage;

              
//               <button type="submit">Signup</button>
//               <br/>
//               <br/>
//               <Link to={"/Login"}>Already a member?</Link>
//             </form>
//           </div>
//         </div>
        
//       </Layout>
      
//     );
//   }
// }

// export default SignupPage;
