export default function Trainer(){
    return(
        <section className="trainer">
        <div className="container">
          <div className="row">
            <div className="img_trainer">
              <img src={require("../images/trainer.png")} />
            </div>
            <p className="trainer_text">
              Lorem ipsum dolor sit amet, consecte-
              <br />
              tur adipiscing elit. Vivamus lacinia odio
              <br /> vitae Vestibulum vestibulum.
            </p>
            <h3 className="heading3">TRAINER DEVANDRA</h3>
          </div>
        </div>
      </section>
    )
}