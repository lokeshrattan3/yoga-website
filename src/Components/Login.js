import React, { useState } from 'react';
import './login.css';
import { Link, useNavigate, useParams } from 'react-router-dom';
import Layout from '../Layout';
import axios from 'axios';
import Cookies from 'js-cookie';

const LoginPage = () => {

  const navigate = useNavigate();

  const userId = Cookies.get('userId');

  axios.defaults.withCredentials = true;


  const [formData, setFormData] = useState({
    email: '',
    password: '',
  });

  const [errors, setErrors] = useState({
    email: '',
    password: '',
  });

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    // Form validation logic
    const { email, password } = formData;
    const newErrors = {};

    if (!email) {
      newErrors.email = 'Email is required';
    }

    if (!password) {
      newErrors.password = 'Password is required';
    }

    if (Object.keys(newErrors).length > 0) {
      setErrors(newErrors);
      return;
    }

    // If validation passes, you can proceed with login logic here
    
    axios.post('http://localhost:5000/yoga-api/login', formData)
      .then((res) => {
        if (res.data.status == "success") {
          alert('Login successful');
          navigate('/');
          window.location.reload(true);
        }else{
          alert(res.data.Error);
        }
        console.log(res);
        console.log('login successful');
      })
      .catch((err) => {
        console.error(err, 'Error in axios request');
      });
  };

  return (
    <Layout>
      <div className="container-fluid" style={{ padding: '5em 0', background: '#6C9894' }}>
        <div className="form-container container-fluid">
          <h2>Login</h2>
          <form onSubmit={handleSubmit} className="container" style={{ boxShadow: 'none' }}>
            <div className="form-group">
              <input
                type="text"
                name="email"
                placeholder="Email"
                value={formData.email}
                onChange={handleInputChange}
              />
              {errors.email && <div className="error">{errors.email}</div>}
            </div>
            <div className="form-group">
              <input
                type="password"
                name="password"
                placeholder="Password"
                value={formData.password}
                onChange={handleInputChange}
                pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$"
                title="Password must contain at least one number, one lowercase letter, one uppercase letter, and be at least 8 characters long."
              />
              {errors.password && <div className="error">{errors.password}</div>}
            </div>
            <button type="submit">Login</button>
            <br />
            <Link to="/signup">Create a new account</Link>
          </form>
        </div>
      </div>
    </Layout>
  );
};

export default LoginPage;
