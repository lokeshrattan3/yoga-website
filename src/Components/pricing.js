import { Link } from "react-router-dom"
export default function Pricing(){
    return(
        <section className="price">
        <div className="container">
          <div className="row">
            <h1 className="heading2">Pricing</h1>
            <p className="text2">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              <br /> Vivamus lacinia odio vitae Vestibulum ves-
              <br />
              tibulum.
            </p>
          </div>
          <div className="row">
            <div className="col-md-4 col-sm-12">
              <div className="newbie">
                <div className="new_text">
                  NEWBIE CLASS
                  <br />
                  $00.00
                </div>
                <div className="vector_img">
                  <img src={require("../images/Vector Smart Object.png")} />
                </div>
                <div className="newbie_text">
                  Lorem ipsum dolor sit amet,
                  <br /> consectetur adipiscing elit.
                </div>
                <div className="new_button">
                  <button>
                  <Link style={{color:"#fff",textDecoration:"none"}}  to="registration">
                  REGISTER NOW
                  </Link>
                  </button>
                </div>
              </div>
            </div>
            <div className="col-md-4 col-sm-12">
              <div className="newbie">
                <div className="new_text">
                  ADVANCED CLASS
                  <br />
                  $00.00
                </div>
                <div className="vector_img">
                  <img src={require("../images/Vector Smart Object.png")} />
                </div>
                <div className="newbie_text">
                  Lorem ipsum dolor sit amet,
                  <br /> consectetur adipiscing elit.
                </div>
                <div className="new_button">
                <button>
                  <Link style={{color:"#fff",textDecoration:"none"}}  to="registration">
                  REGISTER NOW
                  </Link>
                  </button>
                </div>
              </div>
            </div>
            <div className="col-md-4 col-sm-12">
              <div className="expert">
                <div className="new_text">
                  EXPERT CLASS
                  <br />
                  $00.00
                </div>
                <div className="vector_img">
                  <img src={require("../images/Vector Smart Object.png")} />
                </div>
                <div className="newbie_text">
                  Lorem ipsum dolor sit amet,
                  <br /> consectetur adipiscing elit.
                </div>
                <div className="new_button">
                  <button>
                  <Link style={{color:"#fff",textDecoration:"none"}} to="/registration">
                  REGISTER NOW
                  </Link>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
}