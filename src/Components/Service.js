import Layout from "../Layout";
import Class from "./Class";
import Pricing from "./pricing";

export default function Service(){
    return(
        <>
        <Layout>
            <Class/>
            <Pricing/>
        </Layout>
            
        </>
    )
}