// import logo from './logo.svg';
import React from "react";
import { BrowserRouter, Route, Routes} from "react-router-dom";
import "./style.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Home from "./Components/Home";
import About from "./Components/About";
import Service from "./Components/Service";
import Register from "./registration";
import ContactUsForm from "./Components/ContactForm";
import LoginPage from "./Components/Login";
import SignupPage from "./Components/Signup";
import MyProfile from "./Components/MyProfile";
import Cookies from "js-cookie";
import UpdateQuery from "./Components/UpdateQuery";
import PersonalInfo from "./Components/PersonalInfo";
import UpdateInfo from "./Components/UpdateInfo";


function App() {
  
  return (
    <BrowserRouter>
    {/* <div className="container1"> */}
        <Routes>
          {/* <Route  path={isLoggedIn ? `/:id` : "/"} element={<Home/>}/> */}
          <Route  path={"/"} element={<Home/>}/>
          <Route  path="/about" element={<About/>}/>
          <Route  path="/service" element={<Service/>}/>
          <Route  path="/my-profile" element={<MyProfile/>}/>
          <Route  path="/personal-info" element={<PersonalInfo/>}/>
          <Route  path="/update-query/:id" element={<UpdateQuery/>}/>
          <Route  path="/update-info/:id" element={<UpdateInfo/>}/>
          <Route  path="/contact" element={<ContactUsForm/>}/>
          <Route  path="/registeration" element={<Register/>}/>
          <Route  path="/Login" element={<LoginPage/>}/>
          <Route  path="/signup" element={<SignupPage/>}/>
          
        </Routes>
    {/* </div> */}
    </BrowserRouter>
  );
}

export default App;
