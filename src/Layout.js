import React, { useEffect, useState } from 'react'
import './Layout.css'
import Header from './Components/Header'
import Footer from './Components/footer'
import axios from 'axios'
import { Link, useNavigate } from 'react-router-dom'
import { IoMdPerson } from "react-icons/io";
import Cookies from 'js-cookie'


function Layout(props) {
  const [data,setData] = useState({})
  const [show,setShow]= useState(false)
  const [user, setUser] = useState('');
  const navigate = useNavigate();

axios.defaults.withCredentials = true;
  useEffect(()=>{
    axios.get("http://localhost:5000/yoga-api")
    .then(res => {
        if(res.data.status == "success"){
            window.localStorage.setItem("isLoggedIn",true);
            // window.location.reload(true);
            setUser(res.data.name)
        }
    })
  },[])

  const handleLogout = () => {
      axios.get('http://localhost:5000/yoga-api/logout')
      .then((res) => {
        window.localStorage.setItem("isLoggedIn", false);
        window.localStorage.clear();
        window.location.reload(true);
        navigate('/')
      })
      .catch(err => console.log(err))
  }

  // if(reload){
  //   window.location.reload(true);
  // }else{
  //   window.location.reload(false);
  // }
  const isLoggedIn = window.localStorage.getItem("isLoggedIn");

  return (
    <>
        <Header>
                {
                    isLoggedIn ? 
                      <div className="profile" style={{
                          display: "flex", 
                          justifyContent: 'center', 
                          alignItems: 'center',}}>

                          <button 
                            type="button" 
                            className={`btn btn-dark profile-btn `}
                            onClick={()=>setShow((prev)=>!prev)}
                            >
                            <IoMdPerson />
                          </button>

                          <ul className="profile-hover" 
                            style={{display: show ? "flex" : "none",}}
                          >
                            <li className="name-div">Welcome, <span>{user}</span> </li>
                            <li><Link to={`/my-profile`}>My Profile</Link></li>
                            <li>
                            <Link onClick={handleLogout}>
                                Logout
                            </Link>
                            </li>
                          </ul>

                          {console.log(data)}
                      </div>
                    :
                    <Link className="btn btn-dark" to={"/login"}>
                        Login
                    </Link>
                }
                
            </Header>
            {props?.children}
        <Footer/>

        {console.log(isLoggedIn)}
    </>
  )
}

export default Layout